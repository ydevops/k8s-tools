FROM alpine AS downloader

ARG HELM2_VERSION=2.16.6
ARG HELM3_VERSION=3.5.4
ARG KUBECTL_VERSION=1.15.9
ARG HELMFILE_VERSION=0.80.1
ARG KAPP_VERSION=0.9.0
ARG JQ_VERSION=1.6
ARG YTT_VERSION=0.26.0

RUN \
    apk add curl ca-certificates \
    && curl -L https://github.com/roboll/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_linux_amd64 > helmfile \
    && curl -L https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl > kubectl \
    && curl -L https://get.helm.sh/helm-v${HELM2_VERSION}-linux-amd64.tar.gz | tar xvz \
    && mv linux-amd64/helm linux-amd64/helm2 \
    && curl -L https://get.helm.sh/helm-v${HELM3_VERSION}-linux-amd64.tar.gz | tar xvz \
    && mv linux-amd64/helm linux-amd64/helm3 \
    && curl -L https://github.com/k14s/kapp/releases/download/v${KAPP_VERSION}/kapp-linux-amd64 > kapp \
    && curl -L https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 > jq \
    && curl -L https://releases.rancher.com/cli2/v2.2.0/rancher-linux-amd64-v2.2.0.tar.gz | tar xvz \
    && curl -L https://github.com/k14s/ytt/releases/download/v${YTT_VERSION}/ytt-linux-amd64 > ytt \
    && install -m x linux-amd64/helm2 linux-amd64/helm3 kubectl helmfile kapp ytt jq rancher*/rancher /usr/local/bin

FROM ubuntu
ENV CLUSTER_CONFIG_PATH="/opt/klusters/"
WORKDIR /root

COPY --from=downloader /usr/local/bin/* /usr/local/bin/

RUN \
    apt-get update \
    && mkdir .kube \
    && apt-get install -y ca-certificates make git curl vim bash-completion man logrotate \
    && curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt-get update \
    && apt-get install -y nodejs vim python3-pip  \
    && pip3 install jinja2 yq \
    && npm install -g newman newman-reporter-html newman-reporter-htmlextra \
    && update-ca-certificates \
    && ln -sf /usr/local/bin/helm3 /usr/local/bin/helm \
    && VERSION=$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')\
    && curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VERSION/argocd-linux-amd64 \
    && chmod +x /usr/local/bin/argocd \
    && helm repo add rancher-stable https://releases.rancher.com/server-charts/stable \
    # TODO: helm repos add
    && helm repo update \
    && touch .kube/config \
    && apt-get autoremove -y \
    && apt-get clean  \
    && rm -rf /var/lib/apt/lists/*

COPY .bashrc .bashrc
COPY .kube-ps1 .kube-ps1
COPY .prompt_kubectl .prompt_kubectl
COPY render /usr/local/bin
COPY rollout /usr/local/bin
